<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ page import="vo.*"%>
<%@ page import="java.io.*"%>
<%
PrintWriter write = response.getWriter();
Member user = new Member();
user = (Member)session.getAttribute("loginMember");

if(user.getMemberLevel() < 1) {
	write.println("<script>alert('관리자만 접근할 수 있습니다'); location.href='../index.jsp';</script>");
	write.flush();
}

String adminMenu = "";
if (request.getParameter("adminMenu") != null) {
	adminMenu = request.getParameter("adminMenu");
}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet"
		href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
	<link rel="stylesheet" href="../style.css">
</head>
<body>
	<!-- Header Start -->
		<jsp:include page="/partial/header.jsp"></jsp:include>
	<!-- Header End -->
	<!-- submenu Start -->
	<%if(adminMenu.equals("")) {%>
		<jsp:include page="/partial/adminMenu.jsp"></jsp:include>
	<%} else if(adminMenu.equals("selectCategoryList")) {%>
		<jsp:include page="/admin/selectCategoryList.jsp"></jsp:include>
	<%} else if(adminMenu.equals("selectEbookList")) {%>
		<jsp:include page="/admin/selectEbookList.jsp"></jsp:include>
	<%} %>
	<!-- submenu End -->
	
	<!-- jQuery library -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<!-- Popper JS -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>