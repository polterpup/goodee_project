<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="vo.*"%>
<%@ page import="dao.*"%>
<%@ page import="java.io.*"%>
<%
PrintWriter write = response.getWriter();
Member user = new Member();
user = (Member) session.getAttribute("loginMember");

if(user.getMemberLevel() < 1) {
	write.println("<script>alert('관리자만 접근할 수 있습니다'); location.href='../index.jsp';</script>");
	write.flush();
}

int currentPage = 1;
if(request.getParameter("currentPage") != null) {
	currentPage = Integer.parseInt(request.getParameter("currentPage"));
}

final int USER_LIMIT = 10; // ROW_PER_PAGE

MemberDao memberDao = new MemberDao();
ArrayList<Member> memberList = memberDao.selectMemberListAllByPage(currentPage, USER_LIMIT);
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>회원 목록</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet"
		href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
</head>
<body>
	<h1>회원 목록</h1>
	<table border="1" cellpadding="10px" style="text-align: center;">
		<thead>
			<tr>
				<th>memberNo</th>
				<th>memberLevel</th>
				<th>memberName</th>
				<th>memberAge</th>
				<th>memberGender</th>
				<th>createDate</th>
				<th>updateDate</th>
				<th>회원정보</th>
				<th>비밀번호</th>
				<th>회원삭제</th>
			</tr>
		</thead>
		<tbody>
			<%
			for(Member m : memberList) {
			%>
			<tr>
				<td><%=m.getMemberNo()%></td>
				<td>
					<%if(m.getMemberLevel() == 0) { %>
					<span>회원 (<%=m.getMemberLevel()%>)</span>
					<%} else {%>
					<span>관리자 (<%=m.getMemberLevel()%>)</span>
					<%}%> 
				</td>
				<td><%=m.getMemberName()%></td>
				<td><%=m.getMemberAge()%></td>
				<td><%=m.getMemberGender()%></td>
				<td><%=m.getCreateDate()%></td>
				<td><%=m.getUpdateDate()%></td>
				<!-- 특정 회원의 등급을 수정 [현재 0 : 일반회원, 1 : 관리자] -->
				<td><a href="<%=request.getContextPath()%>/admin/updateMemberLevelForm.jsp?memberNo=<%=m.getMemberNo()%>" class="btn btn-success">수정</a></td>
				<!-- 로그인된 관리자의 비밀번호 확인 후 특정회원의 비밀번호를 수정 -->
				<td><a href="<%=request.getContextPath()%>/admin/updateMemberPwForm.jsp?memberNo=<%=m.getMemberNo()%>&memberName=<%=m.getMemberName()%>" class="btn btn-warning">변경</a></td>
				<!-- 로그인된 관리자의 비밀번호를 확인 후 특정회원을 강제 탈퇴 -->
				<td><a href="<%=request.getContextPath()%>/admin/deleteMemberAction.jsp?memberNo=<%=m.getMemberNo()%>" class="btn btn-danger" onclick="if(!confirm('삭제 하시겠습니까?')){return false;}">탈퇴</a></td>
			</tr>
			<%} %>
		</tbody>
	</table>
	<!-- 페이징 추가할 영역 -->
	
	
	<p><a href="<%=request.getContextPath()%>/admin/adminIndex.jsp">관리자 페이지로</a></p>
</body>
</html>