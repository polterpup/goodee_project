<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="vo.*"%>
<%@ page import="dao.*"%>
<%@ page import="java.io.*"%>
<%
PrintWriter write = response.getWriter();
Member user = new Member();
user = (Member) session.getAttribute("loginMember"); // 관리자 관련 코드 짤 때 사용

if (request.getParameter("memberNo") == null) {
	System.out.println("멤버번호 NULL!");
	response.sendRedirect("../index.jsp");
	return;
}
if (request.getParameter("memberNo") == null) {
	System.out.println("멤버이름 NULL!");
	response.sendRedirect("../index.jsp");
	return;
}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>패스워드 수정</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<style>
	body{text-align: center; background-color: #343a40; color: white;}
	body > div{width: 30%; margin: 250px auto;}
	.text-center > a{color: #ffc107;}
	
</style>
</head>
<body>
	<div class="updateLevel-form">
		<form action="updateMemberPwAction.jsp" method="post">
			<h2>패스워드 수정</h2>
			<h3>[ <%=request.getParameter("memberName")%> ]</h3>
			<hr>
			<div class="form-group">
				<input type ="hidden" name="memberNo" value="<%=request.getParameter("memberNo")%>">
				<input type ="hidden" name="memberName" value="<%=request.getParameter("memberName")%>">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text"> 
						<!-- 아이콘 들어가는 자리 -->
						<i class="bi bi-key"></i>
						</span>
					</div>
					<input type="password" class="form-control" name="memberPw"
						placeholder="Password" required="required">
				</div>
			</div>
			<div class="form-group">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text"> 
						<!-- 아이콘 들어가는 자리 -->
						<i class="bi bi-key-fill"></i>
						</span>
					</div>
					<input type="password" class="form-control" name="newMemberPw"
						placeholder="New Password" required="required">
				</div>
			</div>
			<div class="form-group" style="text-align: center;">
				<button type="button" name="submit" class="btn btn-warning btn-lg" onclick="history.back()">뒤로가기</button>
				<button type="submit" name="submit" class="btn btn-success btn-lg">수정</button>
			</div>
		</form>
	</div>
</body>
</html>