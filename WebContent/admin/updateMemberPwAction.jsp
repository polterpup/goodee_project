<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="vo.*"%>
<%@ page import="dao.*"%>
<%
PrintWriter write = response.getWriter();
request.setCharacterEncoding("utf-8");

//Debug Code
System.out.println(request.getParameter("memberNo") + " --> [memberNo]");
System.out.println(request.getParameter("memberPw") + " --> [memberPw]");
System.out.println(request.getParameter("newMemberPw") + " --> [newMemberPw]");

int memberNo = Integer.parseInt(request.getParameter("memberNo"));
String memberName = request.getParameter("memberName");
String memberPw = request.getParameter("memberPw");
String newMemberPw = request.getParameter("newMemberPw");

MemberDao memberDao = new MemberDao();
Member paramMember = new Member();

paramMember.setMemberNo(memberNo);
paramMember.setMemberName(memberName);
paramMember.setMemberPw(memberPw);

boolean updated = memberDao.updateMemberPwByAdmin(paramMember, newMemberPw);

if (updated == true) {
	write.println("<script>alert('비밀번호 수정 완료'); location.href='" + request.getContextPath() +"/admin/selectMemberList.jsp;'</script>");
	write.flush();
} else {
	write.println("<script>alert('없는 회원이거나 비밀번호가 일치하지 않습니다'); history.back();</script>");
	write.flush();
}
%>
