<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="vo.*"%>
<%@ page import="dao.*"%>
<%@ page import="java.io.*"%>
<%
PrintWriter write = response.getWriter();
Member user = new Member();
user = (Member) session.getAttribute("loginMember"); // 관리자 관련 코드 짤 때 사용

if (request.getParameter("memberNo") == null) {
	System.out.println("멤버번호 NULL!");
	response.sendRedirect("../index.jsp");
	return;
}

%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>회원정보 수정</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<style>
	body{text-align: center; background-color: #343a40; color: white;}
	body > div{width: 30%; margin: 250px auto;}
	.text-center > a{color: #ffc107;}
	
</style>
</head>
<body>
	<div class="updateLevel-form">
		<form action="updateMemberLevelAction.jsp" method="post">
			<h2>회원정보 수정</h2>
			<hr>
			<div class="form-group col"">
				<input type ="hidden" name="memberNo" value="<%=request.getParameter("memberNo")%>">
			  	<select class="custom-select" name="memberLevel" required>
				  <option selected value="">등급</option>
				  <option value="1">관리자</option>
				  <option value="0">일반회원</option>
				</select>
			</div>
			<div class="form-group" style="text-align: center;">
				<button type="button" name="submit" class="btn btn-warning btn-lg" onclick="history.back()">뒤로가기</button>
				<button type="submit" name="submit" class="btn btn-success btn-lg">수정</button>
			</div>
		</form>
	</div>
</body>
</html>