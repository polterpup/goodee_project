<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="vo.*"%>
<%@ page import="dao.*"%>
<%@ page import="java.util.*"%>
<%
//한글인코딩
request.setCharacterEncoding("utf-8");
//사용자(일반 회원)들 리스트는 관리자만 출입
//방어코드
Member loginMember = (Member) session.getAttribute("loginMember");
//로그인 멤버값이 없거나 memberLevel이 1미만(일반 사용자)일때는 접근 불가. 순서를 바꾸면안됨(바꾸면 null포인트 인셉션이 일어남).
if (loginMember == null || loginMember.getMemberLevel() < 1) {
	response.sendRedirect(request.getContextPath() + "/index.jsp");
	return;
}

CategoryDao categoryDao = new CategoryDao();
ArrayList<Category> categoryList = categoryDao.selectCategoryList();

int currentPage = 1;
if (request.getParameter("currentPage") != null) {
	currentPage = Integer.parseInt(request.getParameter("currentPage"));
}
System.out.println("[Debug] currentPage : " + currentPage);

final int ROW_PER_PAGE = 10; // rowPerPage변수 10으로 초기화되면 끝까지 10을 써야 한다. --> 상수

int beginRow = (currentPage - 1) * ROW_PER_PAGE;

String ebookCategory = "";

if (request.getParameter("ebookCategory") != null) {
	ebookCategory = request.getParameter("ebookCategory");
}

// 카테고리별 목록 받아오기
EbookDao ebookDao = new EbookDao();
ArrayList<Ebook> ebookList = null;

if (ebookCategory.equals("") == true) {
	ebookList = ebookDao.selectEbookList(beginRow, ROW_PER_PAGE);
} else {
	ebookList = ebookDao.selectEbookListByCategory(beginRow, ROW_PER_PAGE, ebookCategory);
}
%>
<!-- 관리자 메뉴 인클루드 끝 -->
<h1 style="color: white;">[전자책 관리]</h1>
<select name="categoryName" onchange="location.href=this.value">
	<option value="<%=request.getContextPath()%>/admin/adminIndex.jsp?adminMenu=selectEbookList">전체목록</option>
	<%
	for (Category c : categoryList) {
	%>
	<option value="<%=request.getContextPath()%>/admin/adminIndex.jsp?adminMenu=selectEbookList&ebookCategory=<%=c.getCategoryName()%>"><%=c.getCategoryName()%></option>
	<%
	}
	%>
</select>
<a href="adminIndex.jsp">뒤로가기</a>
<!-- 전자책 목록 출력 : 카테고리별 출력 -->
<!-- 전자책 목록 출력 : 카테고리별 출력 -->
<div>
	<table border="1" cellpadding=5 style="color: white; text-align: center; border-color: rgba(245, 222, 179, 0.5);">
		<thead>
			<tr>
				<th>ebookNo</th>
				<th>ebookTitle</th>
				<th>categoryName</th>
				<th>ebookState</th>
			</tr>
		</thead>
		<tbody>
			<%
			for (Ebook e : ebookList) {
			%>
			<tr>
				<td><%=e.getEbookNo()%></td>
				<td><a href="<%=request.getContextPath()%>/admin/selectEbookOne.jsp?ebookNo=<%=e.getEbookNo()%>"><%=e.getEbookTitle()%></a></td>
				<td><%=e.getCategoryName()%></td>
				<td><%=e.getEbookState()%></td>
			</tr>
			<%
			}
			%>
		</tbody>
	</table>

	<!-- 하단 네비게이션 바 -->
	<div >
		<a href="<%=request.getContextPath()%>/admin/adminIndex.jsp?adminMenu=selectEbookList&currentPage=1&ebookCategory=<%=ebookCategory%>">처음으로</a>
		<%
		if (currentPage != 1) {
		%>
		<a href="<%=request.getContextPath()%>/admin/adminIndex.jsp?adminMenu=selectEbookList&currentPage=<%=currentPage - 1%>&ebookCategory=<%=ebookCategory%>">이전</a>
		<%
		}

		int lastPage = ebookDao.selectEbookLastPage(10, ebookCategory);

		int displayPage = 10;

		int startPage = ((currentPage - 1) / displayPage) * displayPage + 1;
		int endPage = startPage + displayPage - 1;

		for (int i = startPage; i <= endPage; i++) {
		if (endPage <= lastPage) {
		%>
		<a
			href="<%=request.getContextPath()%>/admin/adminIndex.jsp?adminMenu=selectEbookList&currentPage=<%=i%>&ebookCategory=<%=ebookCategory%>"><%=i%></a>
		<%
		} else if (endPage > lastPage) {
		%>
		<a
			href="<%=request.getContextPath()%>/admin/adminIndex.jsp?adminMenu=selectEbookList&currentPage=<%=i%>&ebookCategory=<%=ebookCategory%>"><%=i%></a>
		<%
		}

		if (i == lastPage) {
		break;
		}
		}
		if (currentPage != lastPage) {
		%>
		<a
			href="<%=request.getContextPath()%>/admin/adminIndex.jsp?adminMenu=selectEbookList&currentPage=<%=currentPage + 1%>&ebookCategory=<%=ebookCategory%>">다음</a>
		<%
		}
		%>
		<a
			href="<%=request.getContextPath()%>/admin/adminIndex.jsp?adminMenu=selectEbookList&currentPage=<%=lastPage%>&ebookCategory=<%=ebookCategory%>">끝으로</a>
	</div>
</div>