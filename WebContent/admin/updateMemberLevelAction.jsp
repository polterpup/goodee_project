<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="vo.*"%>
<%@ page import="dao.*"%>
<%
PrintWriter write = response.getWriter();
request.setCharacterEncoding("utf-8");

//Debug Code
System.out.println(request.getParameter("memberNo") + " --> [memberNo]");
System.out.println(request.getParameter("memberLevel") + " --> [memberLevel]");

int memberNo = Integer.parseInt(request.getParameter("memberNo"));
int newMemberLevel = Integer.parseInt(request.getParameter("memberLevel"));

MemberDao memberDao = new MemberDao();
Member paramMember = new Member();

paramMember.setMemberNo(memberNo);
paramMember.setMemberLevel(newMemberLevel);

boolean updated = memberDao.updateMemberLevelByAdmin(memberNo, newMemberLevel);

if (updated == true) {
	write.println("<script>alert('등급수정 완료'); location.href='" + request.getContextPath() +"/admin/selectMemberList.jsp;'</script>");
	write.flush();
} else {
	write.println("<script>alert('error!'); history.back();</script>");
	write.flush();
}
%>
