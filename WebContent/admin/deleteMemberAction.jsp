<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="vo.*"%>
<%@ page import="dao.*"%>
<%
//알람창 PrintWriter 클래스 사용 [ IO Package ]
PrintWriter write = response.getWriter();
request.setCharacterEncoding("utf-8");

int memberNo = Integer.parseInt(request.getParameter("memberNo"));

// Debugging..
/*
System.out.println(memberNo);
*/

MemberDao memberDao = new MemberDao();
boolean del_member = memberDao.deleteMemberByAdmin(memberNo);

if (del_member == true) {
	write.println("<script>alert('멤버삭제 완료'); location.href='" + request.getContextPath() +"/admin/selectMemberList.jsp;'</script>");
	write.flush();
} else {
	write.println("<script>alert('error!'); history.back();</script>");
	write.flush();
}
%>