<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div style="color: white;">
	<h1>관리자 페이지 입니다</h1>
	<ul>
		<!-- 
		전차책 카테고리 관리 : 목록, 추가
		1) Category.class
		2) CategoryDao.class
		3) selectCategoryList.jsp (페이징 X)
		4) insertCategoryForm.jsp
		5) insertCategoryAction.jsp
		6) selectCategoryNameCheckAction.jsp
		7) updateCategoryStateAction.jsp
		-->
		<li><a href="<%=request.getContextPath()%>/admin/adminIndex.jsp?adminMenu=selectCategoryList">[전자책 카테고리 관리]</a></li>
		<!-- 전자책 관리 : 목록, 추가(이미지 추가), 수정, 삭제 -->
		<li><a href="<%=request.getContextPath()%>/admin/adminIndex.jsp?adminMenu=selectEbookList">[전자책 관리]</a></li>
		<li><a href="<%=request.getContextPath()%>/admin/selectOrderList.jsp">[주문 관리]</a></li>
		<li><a href="<%=request.getContextPath()%>/admin/">[상품평 관리]</a></li>
		<li><a href="<%=request.getContextPath()%>/admin/">[공지게시판 관리]</a></li>
		<li><a href="<%=request.getContextPath()%>/admin/">[Q&A게시판 관리]</a></li>
		<li><a href="<%=request.getContextPath()%>/index.jsp">홈으로</a></li>
	</ul>
</div>
