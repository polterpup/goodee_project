<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="vo.*"%>
<%@ page import="dao.*"%>
<div class="header">
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand -->
		<a class="navbar-brand" href="<%=request.getContextPath()%>/index.jsp"><i class="bi bi-house-door-fill"></i></a>
		<!-- Links -->
		<ul class="navbar-nav">
			<!-- 로그인이 안되어있을경우 -->
			<%
			if (session.getAttribute("loginMember") == null) {
			%>
			<li class="nav-item header-btns">
				<div class="btn-group">
					<button type="button" class="btn btn-warning dropdown-toggle"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						로그인</button>
					<div class="dropdown-menu">
						<form method="POST" action="./registeration/loginAction.jsp">
							<div>member Id :</div>
							<div>
								<input type="text" name="memberId">
							</div>
							<div>member Pw :</div>
							<div>
								<input type="password" name="memberPw">
							</div>
							<div>
								<button type="submit" class="btn btn-success btn-sm">로그인</button>
							</div>
						</form>
					</div>
				</div>
				<button type="button" class="btn btn-warning" onclick="location.href='<%=request.getContextPath()%>/registeration/insertMemberForm.jsp'">
					회원가입
				</button>
			</li>
			<!-- 로그인 되어있을경우 -->
			<%
			} else {
			Member user = new Member();
			user = (Member) session.getAttribute("loginMember");
			%>
			<li class="nav-item header-btns">
				<span style="color: white; margin-right: 10px;">
					환영합니다 <%=user.getMemberId()%>님, 등급 : 
					<%if(user.getMemberLevel() == 0) { %>
					회원
					<%} else if(user.getMemberLevel() == 1) {%>
					관리자
					<%}%> 
				</span>
				<%if(user.getMemberLevel() == 1) {%>
				<!-- 관리자일 경우 관리자페이지로 이동하는 버튼생성 -->
				<button type="button" class="btn btn-success logoutbtn"
					onclick="location.href='<%=request.getContextPath()%>/admin/selectMemberList.jsp'">
					<i class="bi bi-person-fill"></i>
				</button>
				<%} %>
				<button type="button" class="btn btn-danger logoutbtn"
					onclick="location.href='<%=request.getContextPath()%>/registeration/logout.jsp'">
					<i class="bi bi-box-arrow-right"></i>
				</button>
			</li>
			<%
			}
			%>
		</ul>
	</nav>
</div>