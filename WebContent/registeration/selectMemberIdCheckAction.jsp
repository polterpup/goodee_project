<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.*" %>
<%@ page import="java.io.*"%>
<%
PrintWriter write = response.getWriter();

// memberIdCheck 값이 공백이거나 널인지 -> required로 대체
// MemberDao.selectMemberId() 메서드 호출
String memberIdCheck = request.getParameter("memberIdCheck");

MemberDao memberDao = new MemberDao();
String result = memberDao.selectMemberId(memberIdCheck);

if(result == null) {
	response.sendRedirect(request.getContextPath() + "/registeration/insertMemberForm.jsp?memberIdCheck=" + memberIdCheck);
} else {
	write.println("<script>alert('이미 존재하는 아이디입니다'); location.href='" + request.getContextPath() + "/registeration/insertMemberForm.jsp?checkForm=ON';</script>");
	write.flush();
}

%>
