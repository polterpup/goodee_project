<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
// 인증 방어 코드 : 로그인 전에만 페이지 열람 가능
if (session.getAttribute("loginMember") != null) {
	System.out.println("[Error] : 이미 로그인 되어 있습니다.");
	response.sendRedirect("../index.jsp");
	return;
}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>회원가입</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<style>
	body{text-align: center; background-color: #343a40; color: white;}
	body > div{width: 30%; margin: 250px auto;}
	.text-center > a{color: #ffc107;}
	
</style>
</head>
<body>
	<div class="signup-form">
		<h2>회원 가입</h2>
		<hr>
		<!-- 멤버아이디가 사용가능한지 확인하는 폼 -->
		<!-- 중복확인 버튼을 누르면 checkForm이 ON이 되면서 폼이나옴 -->
		<%
		String checkForm = request.getParameter("checkForm");
		if(checkForm != null && checkForm.equals("ON")) {%>
		<form action="<%=request.getContextPath()%>/registeration/selectMemberIdCheckAction.jsp" method="POST">
			<div class="form-group">
				<div class="input-group mb-3">
					<div class="input-group-prepend">
				    	<span class="input-group-text" id="basic-addon1">
				    		<i class="bi bi-person-fill"></i>
				    	</span>
				  	</div>
				  	<input type="text" class="form-control" placeholder="중복검사할 ID 입력" name="memberIdCheck" required="required">
				 	<div class="input-group-append">
				    	<button type="submit" class="input-group-text">
				    		<i class="bi bi-check-circle"></i>
				    	</button>
				  	</div>
				</div>
			</div>
		</form>
		<%} %>
		<!-- 회원가입 폼 -->
		<form action="<%=request.getContextPath()%>/registeration/insertMemberAction.jsp" method="post">
			<%if(checkForm == null) {%>
			<div class="form-group">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">
						<!-- 아이콘 들어가는 자리 -->
						<i class="bi bi-person-fill"></i>
						</span>
					</div>
					<%if(request.getParameter("memberIdCheck") == null || request.getParameter("memberIdCheck").equals("")) { %>
					<input type="text" class="form-control" name="memberId"
						placeholder="UserName" required="required" readonly="readonly">
					<%} else {%>
					<input type="text" class="form-control" name="memberId" value="<%=request.getParameter("memberIdCheck")%>"
						placeholder="UserName" required="required" readonly="readonly">
					<%} %>
					<div class="input-group-append">
					    <a class="input-group-text" href="<%=request.getContextPath()%>/registeration/insertMemberForm.jsp?checkForm=ON">
							<!-- 아이콘 들어가는 자리 -->
							<i class="bi bi-arrow-right"></i>
						</a>
					</div>
				</div>
			</div>
			<%} %>
			<div class="form-group">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text"> 
						<!-- 아이콘 들어가는 자리 -->
						<i class="bi bi-lock-fill"></i>
						</span>
					</div>
					<input type="password" class="form-control" name="memberPw"
						placeholder="Password" required="required">
				</div>
			</div>
			<hr>
			<div class="form-row">
				<div class="form-group col-md-3"">
				  	<select class="custom-select" name="memberGender" required>
					  <option selected value="">성별</option>
					  <option value="남">남</option>
					  <option value="여">여</option>
					</select>
				</div>
				<input type="text" style="margin-right: 5px;" placeholder="나이" min="0" max="150" maxlength="3" class="form-group form-control col-md-2" name="memberAge" required="required">
				<input type="text" placeholder="별명" class="form-group form-control col" name="memberName" required="required">
			</div>
			<div class="form-group" style="text-align: center;">
				<a href="<%=request.getContextPath()%>/registeration/insertMemberForm.jsp" class="btn btn-danger btn-lg">초기화</a>
				<button type="submit" name="submit" class="btn btn-warning btn-lg">Sign	Up</button>
			</div>
			<div class="text-center">
				이미 계정이 있으신가요? <a href="../index.jsp">로그인 하기</a>
			</div>
		</form>
	</div>
</body>
</html>