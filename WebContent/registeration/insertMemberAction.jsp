<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="vo.*"%>
<%@ page import="dao.*"%>
<%
PrintWriter write = response.getWriter();
request.setCharacterEncoding("utf-8");

// 인증 방어 코드 : 로그인 전에만 페이지 열람 가능
if (session.getAttribute("loginMember") != null) {
	System.out.println("[Error] : 이미 로그인 되어 있습니다.");
	response.sendRedirect("../index.jsp");
	return;
}

// 공백을 equals 할때는 공백을 앞에놓고 비교 : Null 객체에 equals 메서드 호출 시 예외발생
if("".equals(request.getParameter("memberId")) || request.getParameter("memberId") == null) {
	write.println("<script>alert('아이디를 입력 및 중복체크를 해주세요'); location.href='" + request.getContextPath() + "/registeration/insertMemberForm.jsp';</script>");
	write.flush();
	return;
}

// 회원가입 입력 값들이 전부 입력됐는지 유효성 검사 필요X, required로 해결

/* System.out.println(request.getParameter("memberId"));
System.out.println(request.getParameter("memberPw"));
System.out.println(request.getParameter("memberName"));
System.out.println(request.getParameter("memberGender"));
System.out.println(request.getParameter("memberAge")); */

String memberId = request.getParameter("memberId");
String memberPw = request.getParameter("memberPw");
String memberName = request.getParameter("memberName");
String memberGender = request.getParameter("memberGender");
int memberAge = Integer.parseInt(request.getParameter("memberAge"));

// System.out.println(memberId + "\t" + memberPw + "\t" + memberName + "\t" + memberGender + "\t" + memberAge);

MemberDao memberDao = new MemberDao();
Member paramMember = new Member();

paramMember.setMemberId(memberId);
paramMember.setMemberPw(memberPw);
paramMember.setMemberName(memberName);
paramMember.setMemberGender(memberGender);
paramMember.setMemberAge(memberAge);

boolean ins_member = memberDao.insertMember(paramMember);

if (ins_member == true) {
	write.println("<script>alert('가입완료'); location.href='../index.jsp';</script>");
	write.flush();
} else {
	write.println("<script>alert('error!'); location.href='./insertMemberForm.jsp';</script>");
	write.flush();
}
%>