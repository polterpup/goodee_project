package dao;

import java.sql.*;
import java.util.*;

import vo.*;
import commons.*;

public class MemberDao {
	
	// [관리자] 회원Level 수정 | boolean 리턴 : alert창으로 성공 / 실패 여부확인
	public boolean updateMemberLevelByAdmin(int memberNo, int newMemberLevel) throws ClassNotFoundException, SQLException {
		// DB 연결
		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection();
		
		String sql = "UPDATE member SET member_level=?, update_date=NOW() WHERE member_no=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, newMemberLevel);
		stmt.setInt(2, memberNo);
		
		int success = stmt.executeUpdate();
		
		stmt.close();
		conn.close();

		if (success == 1) {
			return true;
		} else {
			return false;
		}
		
		
	}
	
	// [관리자] 회원Pw 수정 : 수정된 Pw 받아와서 수정 | boolean 리턴 : alert창으로 성공 / 실패 여부확인
	public boolean updateMemberPwByAdmin(Member member, String newMemberPw) throws ClassNotFoundException, SQLException {
		// DB 연결
		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection();
		
		String sql = "UPDATE member SET member_pw=SHA2(?,256), update_date=NOW() WHERE member_no=? AND member_name=? AND member_pw=SHA2(?,256)";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, newMemberPw);
		stmt.setInt(2, member.getMemberNo());
		stmt.setString(3, member.getMemberName());
		stmt.setString(4, member.getMemberPw());
		
		int success = stmt.executeUpdate();
		
		stmt.close();
		conn.close();

		if (success == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	// [관리자] 회원삭제 : no 받아와서 삭제 | boolean 리턴 : alert창으로 성공 / 실패 여부확인
	public boolean deleteMemberByAdmin(int memberNo) throws ClassNotFoundException, SQLException {
		// DB 연결
		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection();
		
		PreparedStatement stmt = conn.prepareStatement("DELETE FROM member WHERE member_no=?");
		stmt.setInt(1, memberNo);
		
		int success = stmt.executeUpdate();
		
		// 총 멤버 수
		int totalUser = 0;
		
		// DELETE 시 AUTO_INCREMENT가 안꼬이게 하기위해 초기화 & 재정렬 작업
		PreparedStatement stmt2 = conn.prepareStatement("SELECT COUNT(*) from member");
		ResultSet rs = stmt2.executeQuery();
		if (rs.next()) {
			totalUser = rs.getInt("COUNT(*)");
		}
		
		// 재정렬
		PreparedStatement stmt3 = conn.prepareStatement("set @cnt = 0");
		stmt3.executeQuery();
		
		PreparedStatement stmt4 = conn.prepareStatement("update member set member_no = @cnt:=@cnt+1");
		stmt4.executeQuery();
		
		// 초기화
		PreparedStatement stmt5 = conn.prepareStatement("Alter TABLE member Auto_increment=?");
		stmt5.setInt(1, totalUser);
		stmt5.executeQuery();
		
		rs.close();
		stmt.close();
		stmt2.close();
		stmt3.close();
		stmt4.close();
		stmt5.close();
		conn.close();

		if (success == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	public int selectMemberLastPage(int USER_LIMIT) throws ClassNotFoundException, SQLException {
		int lastPage = 0;
		// DB 연결
		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection();
		
		String sql = "SELECT COUNT(*) from member";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		
		// 총 멤버 수
		int totalUser = 0;
		
		if (rs.next()) {
			totalUser = rs.getInt("COUNT(*)");
		}
		lastPage = totalUser / USER_LIMIT;
		if (totalUser % USER_LIMIT != 0) {
			lastPage += 1;
		}
		
		rs.close();
		stmt.close();
		conn.close();
		
		return lastPage;
	}
	
	// [관리자] 회원목록출력
	public ArrayList<Member> selectMemberListAllByPage (int currentPage, int USER_LIMIT) throws ClassNotFoundException, SQLException{
		ArrayList<Member> list = new ArrayList<Member>();
		
		// DB 연결
		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection();
		
		
		// 페이지 시작 값
		int beginRow = (currentPage - 1) * USER_LIMIT;

		String sql = null;
		PreparedStatement stmt = null;

	
		sql = "SELECT * FROM member ORDER BY member_no DESC limit ?,?";
		stmt = conn.prepareStatement(sql);
		stmt.setInt(1, beginRow);
		stmt.setInt(2, USER_LIMIT);
		

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			Member m = new Member();
			m.setMemberNo(rs.getInt("member_no"));
			m.setMemberLevel(rs.getInt("member_level"));
			m.setMemberName(rs.getString("member_name"));
			m.setMemberAge(rs.getInt("member_age"));
			m.setMemberGender(rs.getString("member_gender"));
			m.setCreateDate(rs.getString("create_date"));
			m.setUpdateDate(rs.getString("update_date"));
			list.add(m);
		}
		
		rs.close();
		stmt.close();
		conn.close();
		
		return list;
	}
	
	// [회원] 멤버 아이디 중복 검사
	public String selectMemberId(String memberIdCheck) throws ClassNotFoundException, SQLException {
		String memberId = null;
		
		// DB
		DBUtil dbUtil = new DBUtil();
		Connection conn = dbUtil.getConnection();
		String sql = "SELECT member_id memberId FROM member WHERE member_id=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, memberIdCheck);
		ResultSet rs = stmt.executeQuery();
		
		if(rs.next()) {
			memberId = rs.getString("memberId");
		}
		
		rs.close();
		stmt.close();
		conn.close();
				
		return memberId;
	}
	
	// [회원]
	public boolean insertMember(Member member) throws ClassNotFoundException, SQLException {
		// DB
		DBUtil dbUtil = new DBUtil();
		Connection conn = dbUtil.getConnection();

		// mysql은 8.x 버전부터 PASSWORD함수를 지원하지 않기 때문에 SHA2(혹은 SHA1, md5와 같은)함수를 사용
		// SHA2(Pw, 길이)와 같이 작성
		String sql = "INSERT INTO member(member_id, member_pw, member_level,"
				+ " member_name, member_age, member_gender, create_date, update_date)"
				+ " VALUES (?, SHA2(?, 256), ?, ?, ?, ?, NOW(), NOW())";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, member.getMemberId());
		stmt.setString(2, member.getMemberPw());
		stmt.setInt(3, 0);
		stmt.setString(4, member.getMemberName());
		stmt.setInt(5, member.getMemberAge());
		stmt.setString(6, member.getMemberGender());

		int success = stmt.executeUpdate();

		stmt.close();
		conn.close();

		if (success == 1) {
			return true;
		} else {
			return false;
		}

	}

	public Member login(Member member) throws ClassNotFoundException, SQLException {
		// DB
		DBUtil dbUtil = new DBUtil();
		Connection conn = dbUtil.getConnection();
		
		// mysql은 8.x 버전부터 PASSWORD함수를 지원하지 않기 때문에 SHA2(혹은 SHA1, md5와 같은)함수를 사용.
		String sql = "SELECT"
				+ " member_id AS memberId,"
				+ " member_name AS memberName,"
				+ " member_level AS memberLevel"
				+ " FROM member"
				+ " WHERE member_id=? AND member_pw=SHA2(?, 256)";
		
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, member.getMemberId());
		stmt.setString(2, member.getMemberPw());
		
		ResultSet rs = stmt.executeQuery();

		if (rs.next()) {
			Member returnMember = new Member();
			returnMember.setMemberId(rs.getString("memberId"));
			returnMember.setMemberName(rs.getString("memberName"));
			returnMember.setMemberLevel(rs.getInt("memberLevel"));
			return returnMember;
		}

		rs.close();
		stmt.close();
		conn.close();
		return null;
	}
}
