package vo;

public class Order {
	private int orderNo;
	private Ebook ebook; // private int ebookNo [SQL의 외래키 때문에 이렇게 사용하지만 상황에 맞게 사용을 목적으로 한다] 
	private Member member; // private int memberNo
	private int orderPrice;
	private String createDate;
	private String updateDate;
}
